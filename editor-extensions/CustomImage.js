import { Node } from 'tiptap'

export const CUSTOM_IMAGE_NAME = 'custom_image'
export const CUSTOM_IMAGE_SCHEMA = {
  attrs: {
    src: {
      default: null,
    },
    width: {
      default: '10%',
    },
  },
  group: 'block',
  draggable: false,
  selectable: false,
  parseDOM: [
    {
      priority: 51,
      tag: 'img',
      getAttrs: (dom) => ({
        src: dom.getAttribute('src'),
        width: dom.getAttribute('width'),
      }),
    },
  ],
  toDOM: (node) => [
    'img',
    {
      src: node.attrs.src,
      width: node.attrs.width,
    },
  ],
}

export default class CustomImage extends Node {
  get name() {
    return CUSTOM_IMAGE_NAME
  }

  get schema() {
    return CUSTOM_IMAGE_SCHEMA
  }

  get view() {
    return {
      props: ['node', 'updateAttrs', 'view'],

      data: () => ({
        displayTrigger: false,
        isResizing: false,
      }),

      computed: {
        width: {
          get() {
            return this.node.attrs.width || '60%'
          },
          set(width) {
            this.updateAttrs({
              width,
            })
          },
        },
        src: {
          get() {
            return this.node.attrs.src
          },
          set(src) {
            this.updateAttrs({
              src,
            })
          },
        },
      },

      methods: {
        activateResize() {
          this.displayTrigger = !this.displayTrigger
        },

        handleMousedown(e) {
          this.isResizing = true

          const startX = e.pageX

          let startWidth
          if (this.node.attrs.width) {
            startWidth = parseFloat(this.node.attrs.width.match(/(.+)%/)[1])
          } else {
            startWidth = 60
          }

          const onMouseMove = (e) => {
            e.preventDefault()
            const currentX = e.pageX

            const diffInPx = currentX - startX
            const diffInEm = diffInPx / 8

            if (startWidth + diffInEm > 60) {
              this.width = '60%'
              return
            }

            if (startWidth + diffInEm < 5) {
              this.width = '5%'
              return
            }

            this.width = `${startWidth + diffInEm}%`
          }

          const onMouseUp = (e) => {
            e.preventDefault()

            document.removeEventListener('mousemove', onMouseMove)
            document.removeEventListener('mouseup', onMouseUp)

            this.displayTrigger = false
          }

          document.addEventListener('mousemove', onMouseMove)
          document.addEventListener('mouseup', onMouseUp)
        },
      },

      template: `
        <div class="py-2 flex justify-center">
          <button
            :style="{ width: this.width }" 
            @click="activateResize"
            class="leading-reset relative focus:ring focus:ring-color-blue-500 focus:outline-none"
          >
            <img class="custom-image__embed" :src="src"></img>
            <button 
              v-if="displayTrigger"
              @mousedown.prevent="handleMousedown"
              class="absolute cursor-move bottom-0 right-0 w-6 h-6 border-t-0 border-l-0 border-r-4 border-b-4 border-yellow-500 focus:outline-none"></button>
          </button>
        </div>
      `,
    }
  }
}
