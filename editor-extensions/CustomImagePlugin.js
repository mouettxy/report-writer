import { Node, Plugin } from 'tiptap'
import { nodeInputRule } from 'tiptap-commands'
import { CUSTOM_IMAGE_SCHEMA, CUSTOM_IMAGE_NAME } from '@/editor-extensions/CustomImage'

const IMAGE_INPUT_REGEX = /!\[(.+|:?)\]\((\S+)(?:(?:\s+)["'](\S+)["'])?\)/

export default class CustomImagePlugin extends Node {
  constructor(uploadFunc = null) {
    super()

    this.uploadFunc = uploadFunc
  }

  get name() {
    return CUSTOM_IMAGE_NAME
  }

  get schema() {
    return CUSTOM_IMAGE_SCHEMA
  }

  commands({ type }) {
    return (attrs) => (state, dispatch) => {
      const { selection } = state
      const position = selection.$cursor ? selection.$cursor.pos : selection.$to.pos
      const node = type.create(attrs)
      const transaction = state.tr.insert(position, node)
      dispatch(transaction)
    }
  }

  inputRules({ type }) {
    return [
      nodeInputRule(IMAGE_INPUT_REGEX, type, (match) => {
        // eslint-disable-next-line
        const [_, alt, src, title] = match
        return {
          src,
          alt,
          title,
        }
      }),
    ]
  }

  get plugins() {
    const upload = this.uploadFunc
    return [
      new Plugin({
        props: {
          handleDOMEvents: {
            handlePaste(view, event, slice) {
              const items = (event.clipboardData || event.originalEvent.clipboardData).items

              console.log(event)

              for (const item of items) {
                if (item.type.indexOf('image') === 0) {
                  event.preventDefault()
                  const { schema } = view.state

                  const image = item.getAsFile()

                  if (upload) {
                    upload(image).then((src) => {
                      const node = schema.nodes[CUSTOM_IMAGE_NAME].create({ src })
                      const transaction = view.state.tr.replaceSelectionWith(node)
                      view.dispatch(transaction)
                    })
                  } else {
                    const reader = new FileReader()
                    reader.onload = (readerEvent) => {
                      const node = schema.nodes[CUSTOM_IMAGE_NAME].create({
                        src: readerEvent.target.result,
                      })
                      const transaction = view.state.tr.replaceSelectionWith(node)
                      view.dispatch(transaction)
                    }
                    reader.readAsDataURL(image)
                  }
                }
              }
            },
            drop(view, event) {
              const hasFiles = event.dataTransfer && event.dataTransfer.files && event.dataTransfer.files.length

              if (!hasFiles) {
                return
              }

              const images = Array.from(event.dataTransfer.files).filter((file) => /image/i.test(file.type))

              if (images.length === 0) {
                return
              }

              event.preventDefault()

              const { schema } = view.state
              const coordinates = view.posAtCoords({ left: event.clientX, top: event.clientY })

              images.forEach(async (image) => {
                const reader = new FileReader()

                if (upload) {
                  const node = schema.nodes[CUSTOM_IMAGE_NAME].create({
                    src: await upload(image),
                  })
                  const transaction = view.state.tr.insert(coordinates.pos, node)
                  view.dispatch(transaction)
                } else {
                  reader.onload = (readerEvent) => {
                    const node = schema.nodes[CUSTOM_IMAGE_NAME].create({
                      src: readerEvent.target.result,
                    })
                    const transaction = view.state.tr.insert(coordinates.pos, node)
                    view.dispatch(transaction)
                  }
                  reader.readAsDataURL(image)
                }
              })
            },
          },
        },
      }),
    ]
  }
}
